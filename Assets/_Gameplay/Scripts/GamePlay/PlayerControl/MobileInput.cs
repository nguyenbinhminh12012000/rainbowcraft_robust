﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MobileInput : MonoBehaviour
{
    [SerializeField] private JoyStick joyStick;
    [SerializeField] private FixedTouchField TouchField;
    [SerializeField] private CharacterControl characterControl;
    private void Update()
    {
        characterControl.RunAxis = joyStick.InputDirection;
        characterControl.LookAxis = TouchField.TouchDist;
    }
}
